# environment setup
Create 'projects' directory in your home folder:
    
    `mkdir ~/projects`

Clone Home-Assistant to ~/project:

    `git clone https://gitlab.com/jairusmartin/home-assistant.git ~/projects/

# service commands

Navigate to project directory:

    `cd ~/projects/home-assistant`

Install Home-Assistant dependencies:

    `./bin/setup.sh`

Exit the shell and log back in for user group Docker to take effect.

Bring up service:

    `./bin/up.sh`

Navigate to the URL using the IP address shown from the up.sh command:

    <ip-address>:8123

**Change user password**

Get into container:

   `docker-compose exec frontend bash`

List users:

   `hass --script auth -c /config list`

Reset password:

   `hass --script auth --config /config change_password <username> <password>`

   Note: Then restart docker-compose down, then docker-compose up -d



