#!/bin/bash
pwd=$(dirname $(readlink -f $0))
wkdir=$(realpath $pwd/..)

echo
docker-compose --project-directory $wkdir up -d
echo
echo "Your IP is `hostname -I`"
echo
docker ps --format "table {{.ID}}\t {{.Names}}\t {{.Status}}\t {{.Image}}\t {{.Ports}}"
